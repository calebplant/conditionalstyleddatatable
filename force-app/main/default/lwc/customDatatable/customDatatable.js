import { LightningElement } from 'lwc';
import LightningDatatable from 'lightning/datatable'
import customNavigationTemplate from './customNavigationType.html'

export default class CustomDatatable extends LightningDatatable {

    static customTypes = {
        navigation: {
            template: customNavigationTemplate,
            typeAttributes: ['label','rowid'],
        }
    };
}