public with sharing class ConditionalOppController {

    // custom metadata type fiels, where clause

    // private static String baseQuery = 'SELECT Id, Name, StageName, Account.Name, AccountId, CloseDate, Amount \n' +
    //                             'FROM Opportunity \n';
    private static Date today = Date.today();
    private static String defaultSettingsRecordName = 'default';
    
    /* Returns a list of ALL available Opportunities 
        Wrapped with whether they are overdue or not (ConditionalOpportunity class)
    */
    @AuraEnabled(cacheable=true)
    public static List<ConditionalOpportunity> getConditionalOpportunities() {
        System.debug('getConditionalOpportunities');
        List<ConditionalOpportunity> result = getPastDueOpportunities(null);
        result.addAll(getCurrentOpportunities(null));
        return result;
    }

    /* Returns a list of Opportunities on specific Account 
        Wrapped with whether they are overdue or not (ConditionalOpportunity class)
    */
    @AuraEnabled(cacheable=true)
    public static List<ConditionalOpportunity> getConditionalOpportunitiesOnAccount(String accountId) {
        String currentId = '' + Id.valueOf(accountId).getSobjectType() == 'Account' ? accountId : null; // Check if it's an Account page
        // System.debug('getConditionalOpportunitiesOnAccount currentId: ' + currentId);
        List<ConditionalOpportunity> result = getPastDueOpportunities(currentId);
        result.addAll(getCurrentOpportunities(currentId));
        return result;
    }

    /* Returns past due Opportunities (Close Date is before today)
        If recordId is present, only queries against a specific Account w/ that Id
    */
    public static List<ConditionalOpportunity> getPastDueOpportunities(String recordId)
    {
        Map<String, String> querySettings = getQuerySettings(null);
        // Fields
        String query = '';
        if(querySettings.get('fields') != null) {
            query = 'SELECT ' + querySettings.get('fields') + ' FROM Opportunity WHERE CloseDate < :today ';
        } else {
            query = 'SELECT Id, Name FROM Opportunity WHERE CloseDate < :today';
        }
        // Filters
        if(querySettings.get('whereClauses') != null) {
            query += querySettings.get('whereClauses');
        }
        if (recordId != null) {
            query += ' AND AccountId=:recordId';
        }
        // System.debug('Query(pastDue): ' + query);
        List<Opportunity> opps = Database.query(query);
        
        List<ConditionalOpportunity> result = new List<ConditionalOpportunity>();
        for(Opportunity eachOpp : opps) {
            result.add(new ConditionalOpportunity(eachOpp, true));
        }
        // System.debug('result: ' + result);
        return result;
    }

    /* Returns current Opportunities (Close Date is today or later)
        If recordId is present, only queries against a specific Account w/ that Id
    */
    public static List<ConditionalOpportunity> getCurrentOpportunities(String recordId)
    {
        Map<String, String> querySettings = getQuerySettings(null);
        String query = '';
        // Fields
        if(querySettings.get('fields') != null) {
            query = 'SELECT ' + querySettings.get('fields') + ' FROM Opportunity WHERE CloseDate >= :today ';
        } else {
            query = 'SELECT Id, Name FROM Opportunity WHERE CloseDate >= :today';
        }
        // Filters
        if(querySettings.get('whereClauses') != null) {
            query += querySettings.get('whereClauses');
        }
        if (recordId != null) {
            query += ' AND AccountId=:recordId';
        }
        // System.debug('Query(current): ' + query);
        List<Opportunity> opps = Database.query(query);


        List<ConditionalOpportunity> result = new List<ConditionalOpportunity>();
        for(Opportunity eachOpp : opps) {
            result.add(new ConditionalOpportunity(eachOpp, false));
        }

        // System.debug('result: ' + result);
        return result;
    }

    /* Fetch custom metadata settings, which determine what fields/where clauses will be included
        If no recordName (metadata record) is given, will use default
    */
    public static Map<String, String> getQuerySettings(String recordName) {
        if(recordName == null) {
            recordName = defaultSettingsRecordName;
        }
        String query = 'SELECT Query_Fields__c, Query_Where_Clauses__c FROM Opportunity_Table_Settings__mdt WHERE DeveloperName=:recordName';
        Opportunity_Table_Settings__mdt[] oppSettings = Database.query(query);

        if(oppSettings.size() > 0) {
            Map<String, String> result = new Map<String, String>();
            // Fields
            if(oppSettings[0].Query_Fields__c != null) {
                result.put('fields', oppSettings[0].Query_Fields__c.deleteWhitespace());
            } else {
                result.put('fields', 'Id,Name');
            }
            // Filters
            result.put('whereClauses', oppSettings[0].Query_Where_Clauses__c);

            // System.debug('querySettings: ' + result);
            return result;
        }
        return new Map<String, String>();
    }

    /* Returns a map of field labels by their name. Example: {StageName: 'Stage', AccountName: 'Account Name'}
        Only returns fields configured in custom metadata settings        
    */
    @AuraEnabled(cacheable=true)
    public static Map<String, String> getFieldLabels() {
        Map<String, String> querySettings = getQuerySettings(null);
        if(querySettings.get('fields') != null) {
            List<String> fields = querySettings.get('fields').split(',');
            Map<String, String> fieldLabelByFieldName = new Map<String, String>();

            // Find Names of parent relationships
            Map<String, Schema.SObjectField> fieldMap = Opportunity.sObjectType.getDescribe().fields.getMap();
            Set<String> relatedParents = new Set<String>();
            for(Schema.SObjectField eachField : fieldMap.values()) {
                if(eachField.getDescribe().getType() == Schema.DisplayType.REFERENCE) {
                    relatedParents.add('' + eachField.getDescribe().getReferenceTo()[0]);
                } 
            }
            // Map field names to their labels
            for(String eachTargetField : fields) {
                String[] fieldString = eachTargetField.split('\\.');
                // Check for parent values. Ex: (Account.Name => {AccountName: 'Account Name'})
                if(relatedParents.contains(fieldString[0])) {
                    fieldLabelByFieldName.put(fieldString[0] + fieldString[1], fieldString[0] + ' ' + fieldString[1]);
                } else {
                    fieldLabelByFieldName.put(eachTargetField, fieldMap.get(eachTargetField).getDescribe().getLabel());
                }
            }
            return fieldLabelByFieldName;
        } else {
            return new Map<String, String>();
        }
    }
}
