import { LightningElement, api } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';

export default class RecordNavigation extends NavigationMixin(LightningElement) {
    @api recordId;
    @api label;

    navigateToRecord() {
        console.log('attempt navigate...ID=');
        console.log(this.recordId);
        
        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                recordId: this.recordId,
                actionName: 'view',
            }
        });
    }
}