import { LightningElement, wire, api } from 'lwc';
import { loadStyle, loadScript } from 'lightning/platformResourceLoader';
import getConditionalOpportunities from '@salesforce/apex/ConditionalOppController.getConditionalOpportunities';
import getConditionalOpportunitiesOnAccount from '@salesforce/apex/ConditionalOppController.getConditionalOpportunitiesOnAccount';
import getFieldLabels from '@salesforce/apex/ConditionalOppController.getFieldLabels';
import MY_TABLE_CSS from '@salesforce/resourceUrl/ConditionalDatatableCSS';

const RED_HIGHLIGHT_ATTRIBUTE = {class: {fieldName: 'cellStyle'}};
const DOWN_ARROW_ICON_ATTRIBUTE = {iconName: {fieldName: 'icon'}, iconPosition: 'left'}
const PAST_DUE_ROW_CLASS = 'pastDueRow';
const DOWN_ARROW_ICON = 'utility:arrowdown';

export default class ConditionalOppDatatable extends LightningElement {

    opportunityData;
    columnData;
    error;

    @api recordId

    sortedBy;
    sortedDirection;

    @wire(getFieldLabels)
    getLabels(response) {
        if(response.data) {
            console.log('fieldLabel response:');
            console.log(response.data);
            this.columnData = this.setColumnData(response.data);
            console.log('parsed data: ');
            console.log(this.columnData);
        }
        if(response.error) {
            console.log('Error fetching field labels');
            console.log(response.error);
        }
    }

    // Invoked when called from a record detail page
    @wire(getConditionalOpportunitiesOnAccount, {accountId: '$recordId'})
    getOpportunities(response) {
        if(response.data) {
            console.log('Response on account:');
            console.log(response.data);
            this.opportunityData = this.setOpportunityData(response.data);
            // console.log('Parsed response:');
            // console.log(this.opportunityData);
        }
        if(response.error) {
            console.log('Error fetching opportunities');
            console.log(response.error);
            this.error = response.error;
        }
    }

    connectedCallback() {
        // Invoked if called from a non-record page
        if(!this.recordId) {
            getConditionalOpportunities()
            .then(result => {
                // console.log('Response NO account:');
                // console.log(result);
                this.opportunityData = this.setOpportunityData(result);
                // console.log('Parsed response:');
                // console.log(this.opportunityData);
            }).catch(error => {
                // console.log('Error fetching opportunities');
                // console.log(error);
                this.error = error;
            })
        }
    }

    renderedCallback() {
        // Load static css (sets row colors)
        Promise.all([
            loadStyle(this, MY_TABLE_CSS),
        ]).then(() => {
            console.log('loaded css!');
        }).catch(error => {
            console.log(error);
        });
    }

    /* Event Handlers */
    updateColumnSorting(event) {
        console.log('==updateColumnSorting==');        
        this.sortedBy = event.detail.fieldName;
        this.sortedDirection = event.detail.sortDirection;
        
        this.sortOpportunityData(event.detail.fieldName, event.detail.sortDirection);
    }

    /* Utilities */
    // Transforms controller response for use in Datatable
    setOpportunityData (data) {
        let result = [];
        console.log('data: ');
        console.log(JSON.parse(JSON.stringify(data)));
        
        // Iterate through opportunities
        data.forEach(eachOpp => {
            let row = {};
            // Highlight row if it's past due
            if(eachOpp.isPastDue) {
                row.cellStyle = PAST_DUE_ROW_CLASS;
                row.icon = DOWN_ARROW_ICON;
            } 
            // Flatten nested fields (Example: Account.Name)
            let flattenedData = {};
            for(const key of Object.keys(eachOpp.opportunity)) {
                if(typeof eachOpp.opportunity[key] === 'object') {
                    for(const innerKey of Object.keys(eachOpp.opportunity[key])) {
                        let field = '' + key + innerKey;
                        let value = eachOpp.opportunity[key][innerKey];
                        flattenedData[field] = value;
                    }
                }
            }
            // console.log('flattenedData:');
            // console.log(flattenedData);
            
            // Add record row
            result.push({...row, ...eachOpp.opportunity, ...flattenedData});
        });
        return result;
    }

    // Set up columns from controller response for use in Datatable
    setColumnData(data) {
        console.log(data);
        let columns = [{label:'', fieldName: 'downArrow', fixedWidth: 34, hideDefaultActions:true, cellAttributes: {...DOWN_ARROW_ICON_ATTRIBUTE, ...RED_HIGHLIGHT_ATTRIBUTE}}];
        for(const key of Object.keys(data)) {
            // console.log(key, data[key]);
            if(key != 'Id') {
                columns.push({label:data[key], fieldName: key, type: 'text', sortable: true, cellAttributes: RED_HIGHLIGHT_ATTRIBUTE})
            }
        }
        return columns;
    }

    // Sorts past due/current opportunities separately, then sets datatable data
    sortOpportunityData(fieldName, sortDirection) {
        console.log('==sortOpportunityData==');

        // Separate into past due and current opportunities
        let pastDueData = [];
        let currentData = [];
        this.opportunityData.forEach(eachOpp => {
            if(eachOpp.cellStyle === PAST_DUE_ROW_CLASS) {
                pastDueData.push(eachOpp);
            } else {
                currentData.push(eachOpp);
            }
        });
        // Sort past/current separately
        var key =(a) => a[fieldName]; 
        var reverse = sortDirection === 'asc' ? 1: -1;
        this.sortData(pastDueData, key, reverse);
        this.sortData(currentData, key, reverse);
        // console.log('result:');
        // console.log([...pastDueData, ...currentData]);

        //Set sorted data (past due first, then current)
        this.opportunityData = [...pastDueData, ...currentData];
    }

    sortData(data, key, reverse) {
        data.sort((a,b) => {
            let valueA = key(a) ? key(a).toLowerCase() : '';
            let valueB = key(b) ? key(b).toLowerCase() : '';            
            return reverse * ((valueA > valueB) - (valueB > valueA));
        });
    }
}