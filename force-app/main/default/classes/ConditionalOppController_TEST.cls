@isTest
public with sharing class ConditionalOppController_TEST {
    
    private static String ACCOUNT_NAME_1 = 'testAccount1';
    private static String ACCOUNT_NAME_2 = 'testAccount2';

    private static Integer PAST_DUE_ON_ACCOUNT_1 = 2;
    private static Integer PAST_DUE_ON_ACCOUNT_2 = 1;
    private static Integer CURRENT_ON_ACCOUNT_1 = 2;
    private static Integer CURRENT_ON_ACCOUNT_2 = 1;

    private static Integer TOTAL_PAST_DUE = PAST_DUE_ON_ACCOUNT_1 + PAST_DUE_ON_ACCOUNT_2;
    private static Integer TOTAL_CURRENT = CURRENT_ON_ACCOUNT_1 + CURRENT_ON_ACCOUNT_2;

    @TestSetup
    static void makeData(){
        /* Accounts */
        List<Account> accs = new List<Account>();
        Account acc1 = new Account(Name=ACCOUNT_NAME_1);
        Account acc2 = new Account(Name=ACCOUNT_NAME_2);
        insert acc1;
        insert acc2;

        /* Opportunities */
        List<Opportunity> opps = new List<Opportunity>();
        Date today = Date.today();
        // Past Due Opportunities
        opps.add(new Opportunity(Name='overdue -10', CloseDate=today.addDays(-10), StageName='Prospecting', AccountId=acc1.id));
        opps.add(new Opportunity(Name='overdue -20', CloseDate=today.addDays(-20), StageName='Prospecting', AccountId=acc1.id));
        opps.add(new Opportunity(Name='overdue -30', CloseDate=today.addDays(-30), StageName='Prospecting', AccountId=acc2.id));
        // Current Opportunities
        opps.add(new Opportunity(Name='current 10', CloseDate=today.addDays(10), StageName='Prospecting', AccountId=acc1.id));
        opps.add(new Opportunity(Name='current 20', CloseDate=today.addDays(20), StageName='Prospecting', AccountId=acc1.id));
        opps.add(new Opportunity(Name='current 30', CloseDate=today.addDays(30), StageName='Prospecting', AccountId=acc2.id));
        insert opps;
    }

    @isTest
    static void doesGetConditionalOpportunitiesGetAllResults() {
        Test.startTest();
        List<ConditionalOpportunity> actual = ConditionalOppController.getConditionalOpportunities();
        Test.stopTest();

        List<ConditionalOpportunity> actualPastDue = new List<ConditionalOpportunity>();
        List<ConditionalOpportunity> actualCurrent = new List<ConditionalOpportunity>();
        for(ConditionalOpportunity eachOpp : actual) {
            if(eachOpp.isPastDue == true) {
                actualPastDue.add(eachOpp);
            } else {
                actualCurrent.add(eachOpp);
            }
        }
        System.assertEquals(TOTAL_PAST_DUE, actualPastDue.size());
        System.assertEquals(TOTAL_CURRENT, actualCurrent.size());
    }

    @isTest
    static void doesGetConditionalOpportunitiesOnAccountReturnAccountSpecific() {
        Account acc1 = [SELECT Id FROM Account WHERE Name=:ACCOUNT_NAME_1];
        Test.startTest();
        List<ConditionalOpportunity> actual = ConditionalOppController.getConditionalOpportunitiesOnAccount(acc1.Id);
        Test.stopTest();

        List<ConditionalOpportunity> actualPastDue = new List<ConditionalOpportunity>();
        List<ConditionalOpportunity> actualCurrent = new List<ConditionalOpportunity>();
        for(ConditionalOpportunity eachOpp : actual) {
            if(eachOpp.isPastDue == true) {
                actualPastDue.add(eachOpp);
            } else {
                actualCurrent.add(eachOpp);
            }
        }
        System.assertEquals(PAST_DUE_ON_ACCOUNT_1, actualPastDue.size());
        System.assertEquals(CURRENT_ON_ACCOUNT_1, actualCurrent.size());
    }
}
