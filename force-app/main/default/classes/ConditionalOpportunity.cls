public with sharing class ConditionalOpportunity {
    @AuraEnabled
    public Opportunity opportunity;
    @AuraEnabled
    public Boolean isPastDue;

    public ConditionalOpportunity(Opportunity opp, Boolean pastDue) {
        this.opportunity = opp;
        this.isPastDue = pastDue;
    }
}
