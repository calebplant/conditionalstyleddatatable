import { LightningElement, wire } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent'
import getObjectFields from '@salesforce/apex/modifyConfiguredFieldsController.getObjectFields';
import updateMetadata from '@salesforce/apex/modifyConfiguredFieldsController.updateMetadata';
import getQuerySettings from '@salesforce/apex/ConditionalOppController.getQuerySettings';

const DEFAULT_RECORD_NAME = 'default';

export default class ModifyConfiguredFields extends LightningElement {

    fieldNames;
    whereClauses;
    selectedFields;
    error;

    @wire(getObjectFields, {apiName: 'Opportunity'})
    getFieldOptions(response) {
        if(response.data) {
            // console.log('response:');
            // console.log(response.data);
            this.fieldNames = response.data.map(eachField => {
                return {label: eachField, value: eachField};
            });
            console.log('Retrieved field names: ');
            console.log(this.fieldNames);
        }
        if(response.error) { 
            console.log('Error fetching fields: ');
            console.log(response.error);
            this.error = response.error;
        }
    }

    @wire(getQuerySettings, {recordName: DEFAULT_RECORD_NAME})
    getCurrentSettings(response) {
        if(response.data) {
            console.log('response: ');
            console.log(response.data);
            this.whereClauses = response.data.whereClauses;
            this.selectedFields = response.data.fields.split(',');
        }
        if(response.error) { 
            console.log('Error fetching fields: ');
            console.log(response.error);
            this.error = response.error;
        }
    }

    handleChange(event) {
        this.selectedFields = event.detail.value;
        // console.log('selected fields: ');
        // console.log(this.selectedFields);
    }

    handleSaveChanges() {
        let fieldNames = this.selectedFields.join(',');
        // console.log('fieldNames:');
        // console.log(fieldNames);
        
        updateMetadata({recordName: DEFAULT_RECORD_NAME, fieldNames: fieldNames, whereClauses: this.whereClauses})
        .then(result => {
            console.log('Successfully called updateMetadata()');
            if(result) {
                const successEvent = new ShowToastEvent({
                    title: 'Success',
                    variant: 'success',
                    message: 'Successfully enqueued changes. They should be done almost immediately.',
                });
                this.dispatchEvent(successEvent);
            } else {
                const queryErrorEvent = new ShowToastEvent({
                    title: 'Failure',
                    variant: 'error',
                    message: 'Error validating query. Please double check your Where Clauses',
                });
                this.dispatchEvent(queryErrorEvent);
            }
        })
        .catch(error => {
            console.log('Error calling updateMetadata()!');
            console.log(error);

            const event = new ShowToastEvent({
                title: 'Failure',
                variant: 'error',
                message: 'There was an issue deploying your changes. Please try again or contact a developer.',
            });
            this.dispatchEvent(event);
        })
    }

}