public with sharing class modifyConfiguredFieldsController {

    private static Date today = Date.today();

    // Returns list of fields for passed Object
    @AuraEnabled(cacheable=true)
    public static List<String> getObjectFields(String apiName)
    {
        List<String> fields = new List<String>();

        SObjectType recordType = ((SObject)(Type.forName('Schema.'+apiName).newInstance())).getSObjectType();
        Map<String,Schema.SObjectField> fieldMap = recordType.getDescribe().fields.getMap();

        for (Schema.SObjectField eachField : fieldMap.values()) {
            Schema.DescribeFieldResult field = eachField.getDescribe();
            fields.add('' + eachField);
        }
        fields.sort();
        return fields;
    }

    // Adds/Modifies Opportunity Table Settings record
    @AuraEnabled
    public static Boolean updateMetadata(String recordName, String fieldNames, String whereClauses)
    {
        if(isValidQuery(fieldNames, whereClauses)) {
            // Create the metadata type
            Metadata.CustomMetadata oppSettingsRecord = new Metadata.CustomMetadata();
            oppSettingsRecord.fullName = 'Opportunity_Table_Settings.' + recordName;
            oppSettingsRecord.label = recordName;
            
            // Set field values
            Metadata.CustomMetadataValue queryFieldsField = new Metadata.CustomMetadataValue();
            queryFieldsField.field = 'Query_Fields__c';
            queryFieldsField.value = fieldNames;
            Metadata.CustomMetadataValue whereClauseField = new Metadata.CustomMetadataValue();
            whereClauseField.field = 'Query_Where_Clauses__c';
            whereClauseField.value = whereClauses;
            // Add field values
            oppSettingsRecord.values.add(queryFieldsField);
            oppSettingsRecord.values.add(whereClauseField);
            
            // Deploy changes
            Metadata.DeployContainer mdContainer = new Metadata.DeployContainer();
            mdContainer.addMetadata(oppSettingsRecord);
            System.debug('New metadata: ' + oppSettingsRecord);
            CustomMetadataCallback callback = new CustomMetadataCallback();
            Id jobId = Metadata.Operations.enqueueDeployment(mdContainer, callback);
            return true;
        } else {
            return false;
        }
        
    }

    private static Boolean isValidQuery(String fieldNames, String whereClauses) {
        String query = 'SELECT ' + fieldNames + ' FROM Opportunity WHERE CloseDate >= :today ';
        if(whereClauses != null) {
            query += whereClauses;
        }
        System.debug('New Query: ' + query);
        try {
            List<Opportunity> opps = Database.query(query);
            return true;
        } catch(QueryException e) {
            return false;
        }
    }
}
