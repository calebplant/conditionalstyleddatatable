# Conditionally Styled Datatable

## Quick Demo

When **not** on an Account record page, it pulls opportunities from all available Accounts.

![Sorting](media/demo1.gif)

When on an Account record page, it pulls opportunities only for that Account

![Account Page](media/demo2.gif)

## Class Overview

### LWC

* **conditionalOppDatatable** - Main component that sets up datatable columns, invokes a controller to get opportunities, then populates the rows/sorts as needed
* **customDatatable** and **recordNavigation** - Supporting components so we can add a new type to our datatable inside **conditionalOppDatatable**. This let's us hyperlink with NavigationMixin instead of URLs.

### Static Resource

* **ConditionalDatatableCSS.css** - css that we load into **conditionalOppDatatable** after rendering, so that we can apply the proper styling on past due rows